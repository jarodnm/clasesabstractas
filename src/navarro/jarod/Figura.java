package navarro.jarod;

public abstract class Figura {

    protected double area;
    protected double perimetro;
    
    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }

    public abstract void calcularArea();

    public abstract void calcularPerimetro();

    @Override
    public String toString() {
        return
                "area: " + area +
                        ", perimetro: " + perimetro
                ;
    }
}

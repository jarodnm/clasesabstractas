package navarro.jarod;

import java.io.*;
import java.util.ArrayList;

public class UI {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ArrayList<Figura> figuras = new ArrayList<>();
    public static void main(String[] args) throws IOException {
        boolean noSalir = true;
        do {
            out.println("Digite su opcion");
            out.println("1.Circulo");
            out.println("2.Rectangulo");
            out.println("3.Triangulo");
            out.println("4.Listar Figuras");
            out.println("5.Salir");
            int opcion = Integer.parseInt(in.readLine());
            noSalir = procesarOpcion(opcion);
        } while (noSalir);
    }

    public static boolean procesarOpcion(int opcion)throws IOException {
        boolean noSalir = true;
        switch (opcion) {
            case 1:
                circulo();
                break;
            case 2:
                rectangulo();
                break;
            case 3:
                triangulo();
                break;
            case 4:
                listarFiguras();
                break;
            case 5:
                out.println("Gracias por usar el programa");
                noSalir = false;
                break;
            default:
                out.println("Digite una opcion correcta");
                break;
        }
        return noSalir;
    }

    public static void circulo()throws IOException{
        double radio;
        out.println("Digite el radio");
        radio = Double.parseDouble(in.readLine());
        Circulo tmpcirculo = new Circulo(radio);
        out.println(tmpcirculo.toString());
        figuras.add(tmpcirculo);
    }

    public static void rectangulo()throws IOException{
        double largo;
        double ancho;
        out.println("Digite el largo");
        largo = Double.parseDouble(in.readLine());
        out.println("Digite el ancho");
        ancho = Double.parseDouble(in.readLine());
        Rectangulo tmprectangulo = new Rectangulo(largo,ancho);
        out.println(tmprectangulo.toString());
        figuras.add(tmprectangulo);
    }

    public static void triangulo()throws IOException{
        double base;
        double altura;
        double ladoa;
        double ladob;

        out.println("Digite la base");
        base = Double.parseDouble(in.readLine());
        out.println("Digite la altura");
        altura = Double.parseDouble(in.readLine());
        out.println("Digite el primer lado");
        ladoa = Double.parseDouble(in.readLine());
        out.println("Digite el segundo lado");
        ladob = Double.parseDouble(in.readLine());
        Triangulo tmptriangulo = new Triangulo(base,altura,ladoa,ladob);
        out.println(tmptriangulo.toString());
        figuras.add(tmptriangulo);
    }

    public static void listarFiguras()throws IOException{
        for(Figura dato:figuras){
            out.println(dato.toString());
        }
    }
}

package navarro.jarod;

public class Triangulo extends Figura {

    private double base;
    private double altura;
    private double ladoA;
    private double ladoB;

    public Triangulo() {
        super();
    }

    public Triangulo(double base, double altura , double ladoA, double ladoB) {
    this.base = base;
    this.altura = altura;
    this.ladoA = ladoA;
    this.ladoB = ladoB;
    calcularPerimetro();
    calcularArea();
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getLadoA() {
        return ladoA;
    }

    public void setLadoA(double ladoA) {
        this.ladoA = ladoA;
    }

    public double getLadoB() {
        return ladoB;
    }

    public void setLadoB(double ladoB) {
        this.ladoB = ladoB;
    }

    public void calcularArea() {
        double area = 0;
        area = (base*altura)/2;
        super.setArea(area);
    }

    public void calcularPerimetro(){
        double perimetro = 0;
        perimetro = ladoA + ladoB + base;
        super.setPerimetro(perimetro);
    }

}

package navarro.jarod;

public class Circulo extends Figura {
    private double radio;

    public Circulo() {
        super();
    }

    public Circulo(double radio) {
        this.radio = radio;
        calcularPerimetro();
        calcularArea();
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }


    public void calcularArea() {
        double area = 0;
        area = Math.PI * (Math.pow(radio,2));
        super.setArea(area);
    }


    public void calcularPerimetro(){
        double perimetro = 0;
        perimetro = 2*Math.PI*radio;
        super.setPerimetro(perimetro);
    }
}
